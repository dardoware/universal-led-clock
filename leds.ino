void fade_m() { for(int i = 0; i < NUM_LEDS_M; i++) { leds_m[i].nscale8(250); } }

void fade_h() { for(int i = 0; i < NUM_LEDS_H; i++) { leds_h[i].nscale8(250); } }

void fadeTodo() { 
  if (CLOCK_TYPE==RELOJ_2_AROS) { 
    fade_m();
    fade_h();
  } else {
    fade_m();
  }
}

void apagar_m() { for(int i = 0; i < NUM_LEDS_M; i++) { leds_m[i]= CRGB(0,0,0); } }
void apagar_h() { for(int i = 0; i < NUM_LEDS_H; i++) { leds_h[i]= CRGB(0,0,0); } }

void apagarTodo() {
  if (CLOCK_TYPE==RELOJ_2_AROS) { 
    apagar_m();
    apagar_h();
  } else {
    apagar_m();
  }
}

void reloj_solido() {
  apagarTodo();
  leds_m[minute()%60]=c_minutos;
  leds_m[second()%60]=c_segundos;
  if (CLOCK_TYPE == RELOJ_2_AROS) {
    leds_h[(hour()%12)]=c_horas;
  } else {
    leds_m[(5*hour())%60]=c_horas;
  }
}

void reloj_gradual() {
  fadeTodo();
   if (CLOCK_TYPE==RELOJ_2_AROS) {
     leds_h[(hour()%12)]=c_horas; 
   }else {
     leds_m[(5*hour())%60]=c_horas; 
   }
  leds_m[minute()]=c_minutos; 
  leds_m[second()%60]=c_segundos;
}

void reloj_arcos() {
  apagarTodo();
  if (CLOCK_TYPE==RELOJ_2_AROS) {
    for (uint8_t h=0; h<12; h++) {
      if (h<= hour()%12) {
        leds_h[h%12 ]=c_horas;
      }
    }
  } else {
    for (uint8_t h=0; h<60; h++) {
      if ((h<= 5*(hour()%12))%60 ) {
        leds_m[h]=c_horas;
      }
    }
  }
  
  for (uint8_t m=0; m<60; m++) {
    if (m<= minute()) {
      leds_m[m]+=c_minutos;
    }
  }
}

void reloj_sombra() {
  if (CLOCK_TYPE == RELOJ_2_AROS) {
    for (uint8_t i=0; i<NUM_LEDS_H; i++) {
      leds_h[i]=c_horas;
    }
    for (uint8_t i=0; i<NUM_LEDS_M; i++) {
      leds_m[i]=c_minutos;
    }
  
  } else {
    for (uint8_t i=0; i<NUM_LEDS_M; i++) {
      leds_m[i]=c_horas;
    }
  }  
 
  leds_m[minute()%60]=CRGB(0,0,0);
  leds_m[second()%60]=CRGB(0,0,0);
  if (CLOCK_TYPE == RELOJ_2_AROS) {
    leds_h[(hour()%12)]=CRGB(0,0,0);
  } else {
    leds_m[(5*hour())%60]=CRGB(0,0,0);
  }
}
