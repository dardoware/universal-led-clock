void setPaletaParchis() {
  c_horas=CRGB(255,0,0);
  c_minutos=CRGB(0,0,255);
  c_segundos=CRGB(128,128,0);
  c_auxiliar=CRGB(0,255,0);
}

void setPaletaFuego() {
  c_horas=CHSV(HUE_RED,255,255);
  c_minutos=CHSV(HUE_YELLOW,255,255);
  c_segundos=CHSV(HUE_ORANGE,255,128);
  c_auxiliar=CRGB(128,128,128);
}

void setPaletaAgua() {
  c_horas=CHSV(HUE_BLUE,255,255);
  c_minutos=CHSV(HUE_AQUA,255,255);
  c_segundos=CHSV(HUE_BLUE,255,128);
  c_auxiliar=CRGB(128,128,128);
}

void selectPaleta() {
  switch(paleta) {
    case P_PARCHIS:
      setPaletaParchis();
      break;
    case P_FUEGO:
      setPaletaFuego();
      break;
    case P_AGUA:
      setPaletaAgua();
      break;
    default:
      paleta=P_PARCHIS;
      break;
  }
}
