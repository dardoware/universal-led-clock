void leerMando() {
  static unsigned int comando_anterior;
  unsigned int comando = lectura.value;
  switch(comando) {
    case REPETICION:
      switch (comando_anterior) {
        case BRILLO_PLUS: 
          subirBrillo();
          break;
        case BRILLO_MENOS:
          bajarBrillo();
          break;
      }
      break;
    case NEXT_PALETA:
      avanzarPaletaAros();
      break;
    case NEXT_MODO: 
      avanzarModoAros();
      break;
    case BRILLO_PLUS:
      subirBrillo();
      break;
    case BRILLO_MENOS:
      bajarBrillo();
      break;
    default: break;
  }
  if (comando != REPETICION) {
    comando_anterior = comando;
  }
}

