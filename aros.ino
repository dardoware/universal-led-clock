void actualizaRelojAros() {
  switch(modo_aros) {
  case M_SOLIDO:
    reloj_solido();
    FPS=60;
    break;
  case M_GRADUAL:
    reloj_gradual();
    FPS=60;
    break;
  case M_ARCOS:
    reloj_arcos();
    FPS=60;
    break;
  case M_SOMBRA:
    reloj_sombra();
    FPS=60;
    break;
  default: 
    modo_aros=M_SOLIDO; 
    Serial.println("fallo");
    break;
  }
}

