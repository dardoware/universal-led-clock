/*
 * Smirjl's Universal Clock
 * 
 */
#include <FastLED.h>
#include <TimeLib.h>
#include <EEPROM.h>
#include <Bounce2.h>
#include <IRremote.h>
#include "IR_codes.h"


// Clase de hardware para mostrar la hora

#define RELOJ_2_AROS     0  // Un aro de 60 leds para minutos y segundos y un aro de doce leds para la hora
#define RELOJ_1_ARO      1  // Un aro de 60 leds para todo
#define RELOJ_7_SEGMENT  2  // Displays de 7 Segmentos
#define RELOJ_RGB_MAT_21 3  // Una matrix RGB
#define RELOJ_DOT_MATRIX 4  // Una matrix de leds monócroma
#define RELOJ_7_SEG_ARO  5  // Display de 7 segmebtos y aro
#define RELOJ_RGB_MAT_11 6

#define CLOCK_TYPE RELOJ_1_ARO   // Elije uno de los tipos de hardware de arriba

// Configuraciones 

boolean DEPURAR         = true;   // Mensajes de depuracion por el puerto de serie
boolean SERIAL_SYNC     = true;   // Recibir la hora por el puerto de serie
boolean USAR_MANDO      = true;  // Usar Mando a distancia por IR
boolean USAR_BOTON      = false;   // Usar botones fñisicos
boolean USAR_TACTILES   = false;  // Usar botones capacitivos
boolean USAR_SENSOR_LUZ = false;  // Usar sensor de nivel de luz

// mapa de pines

#define PIN_BOTON_MODO   5
#define PIN_LED_H        6
#define PIN_LED_M        7
#define PIN_BOTON_PALETA 8
#define PIN_RECIBIR_IR   9
#define PIN_SENSOR_LUZ   A0

// Memory map

const uint8_t MODE_ADDRESS = 0; 
const uint8_t PALETA_ADDRESS = 1;
const uint8_t BRILLO_ADDRESS = 2;

// Modos reloj dos aros

const uint8_t M_SOLIDO    = 0;
const uint8_t M_GRADUAL   = 1;
const uint8_t M_ARCOS     = 2;
const uint8_t M_SOMBRA    = 3;
const uint8_t TOTAL_MODOS = 4;

// Paletas reloj un aro y dos aros

const uint8_t P_PARCHIS = 0;
const uint8_t P_FUEGO = 1;
const uint8_t P_AGUA = 2;
const uint8_t TOTAL_PALETAS = 3;

uint8_t modo_aros= 0;
uint8_t paleta=0; 

// Variables relacionadas con los píxeles RGB

CRGB c_horas=CRGB(255,0,0);
CRGB c_minutos=CRGB(0,255,0);
CRGB c_segundos=CRGB(0,0,255);
CRGB c_auxiliar=CRGB(128,0,128);

const uint8_t  NUM_LEDS_H = 12;
const uint8_t  NUM_LEDS_M = 60;

CRGB leds_h[NUM_LEDS_H];
CRGB leds_m[NUM_LEDS_M];

unsigned int FPS = 120;

unsigned int brillo = 64;

const uint8_t BRILLO_MAX = 255;
const uint8_t BRILLO_MIN = 64;

// Variables relacionadas con la matrix RGB

boolean filasInvertidas= false;

const uint8_t FILAS_MATRIX    = 11;
const uint8_t COLUMNAS_MATRIX = 11;

// Temporizador para refrescar los mensajeas de depuración sin saturar la salida de serie

long debugTimerOld = 0;
int debugTime = 1000;

// Variables relacionadas con los botones

Bounce boton_modo = Bounce();
Bounce boton_paleta = Bounce();

// Variables relacionadas con el mando a distancia

 IRrecv irrecv(PIN_RECIBIR_IR);
 decode_results lectura;

void setup()  {
  if (USAR_BOTON) {
    pinMode(PIN_BOTON_MODO,INPUT_PULLUP);
    pinMode(PIN_BOTON_PALETA,INPUT_PULLUP);
    boton_modo.attach(PIN_BOTON_MODO);
    boton_modo.interval(5);
    boton_paleta.attach(PIN_BOTON_PALETA);
    boton_paleta.interval(5);
  }
  
  if (USAR_MANDO) {
      irrecv.enableIRIn();
      delay(0);
  }
  
  modo_aros = EEPROM.read(MODE_ADDRESS);
  paleta = EEPROM.read(PALETA_ADDRESS);
  brillo = EEPROM.read(BRILLO_ADDRESS);
  
  setSyncProvider(getTeensy3Time);
  
  if (DEPURAR || SERIAL_SYNC) {
    Serial.begin(115200);
  }

  
  if (SERIAL_SYNC) {
    while (!Serial);  
      delay(100);
    if (timeStatus()!= timeSet) {
      Serial.println("Unable to sync with the RTC");
    } else {
      Serial.println("RTC has set the system time");
    }
  }
  
  if (CLOCK_TYPE==RELOJ_2_AROS || CLOCK_TYPE==RELOJ_1_ARO) {
    if (CLOCK_TYPE==RELOJ_2_AROS) {
      LEDS.addLeds<WS2812B,PIN_LED_H,GRB>(leds_h,NUM_LEDS_H);
    }
    LEDS.addLeds<WS2812B,PIN_LED_M,GRB>(leds_m,NUM_LEDS_M);
    LEDS.setBrightness(brillo);

    selectPaleta();
  }
  
  if (DEPURAR) {
    Serial.println("It's show time!");
  }
}

void loop() {
  if (SERIAL_SYNC) {
    serial_sync_clock();
  }

  if (DEPURAR) {
    unsigned long debugTimer = millis();
    if (debugTimer - debugTimerOld > debugTime) {
      debugTimerOld = debugTimer;
      digitalClockDisplay();
    }
  }

  if (USAR_BOTON) {
    boton_modo.update();
    boton_paleta.update();
  
    if (boton_modo.fell()) {
      avanzarModoAros();
    }

    if (boton_paleta.fell()) {
      avanzarPaletaAros();
    }
  }
  
  if (CLOCK_TYPE == RELOJ_2_AROS || CLOCK_TYPE == RELOJ_1_ARO) {
    actualizaRelojAros();
  }

  
  if (USAR_MANDO) {
    if (irrecv.decode(&lectura)) {
      leerMando();
      irrecv.resume();
    }
  }

  if (CLOCK_TYPE == RELOJ_2_AROS || CLOCK_TYPE==RELOJ_1_ARO) {
    FastLED.show();
    LEDS.setBrightness(brillo);
    FastLED.delay(1000/FPS);
  }
}

void serial_sync_clock() {
  if (Serial.available()) {
    time_t t = processSyncMessage();
    if (t != 0) {
      Teensy3Clock.set(t); // set the RTC
      setTime(t);
    }
  }
}

void digitalClockDisplay() {
  // digital clock display of the time
  Serial.print(hour());
  printDigitos(minute());
  printDigitos(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print("/");
  Serial.print(month());
  Serial.print("/");
  Serial.print(year()); 
  Serial.println(); 
}

time_t getTeensy3Time()
{
  return Teensy3Clock.get();
}


