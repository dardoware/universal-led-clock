/*  code to process time sync messages from the serial port   */
#define TIME_HEADER  "T"   // Header tag for serial time sync message

unsigned long processSyncMessage() {
  unsigned long pctime = 0L;
  const unsigned long DEFAULT_TIME = 1357041600; // Jan 1 2013 

  if(Serial.find(TIME_HEADER)) {
     pctime = Serial.parseInt();
     return pctime;
     if( pctime < DEFAULT_TIME) { // check the value is a valid time (greater than Jan 1 2013)
       pctime = 0L; // return 0 to indicate that the time is not valid
     }
  }
  return pctime;
}

void printDigitos(int digitos){

  Serial.print(":");
  if(digitos < 10)
    Serial.print('0');
  Serial.print(digitos);
}

void avanzarPaletaAros() {
  if (paleta < TOTAL_PALETAS -1 ) {
    paleta=paleta+1;
  } else {
    paleta = 0; 
  }
  EEPROM.update(PALETA_ADDRESS,paleta);
  if (DEPURAR) {
    Serial.print("Paleta ");Serial.print(paleta);Serial.print(" guardado en posición ");Serial.println(PALETA_ADDRESS);
  }
  selectPaleta();
}

void avanzarModoAros() {
  if (modo_aros < TOTAL_MODOS -1) {
    modo_aros=modo_aros+1;
  } else {
    modo_aros = 0;
  }
  EEPROM.update(MODE_ADDRESS,modo_aros);
  if (DEPURAR) {
    Serial.print("Modo ");Serial.print(modo_aros);Serial.print(" guardado en posición ");Serial.println(MODE_ADDRESS);
  }
}

void subirBrillo() {
  if (brillo < BRILLO_MAX) {
    brillo+= 16;
  } else {
    brillo = BRILLO_MAX;
  }
  Serial.print("Subiendo brillo");
  Serial.println(brillo);
}

void bajarBrillo() {
  if (brillo > BRILLO_MIN) {
    brillo -= 16;
  } else {
    brillo = BRILLO_MIN;
  }
  Serial.print("Bajando brillo");
  Serial.println(brillo);
}
