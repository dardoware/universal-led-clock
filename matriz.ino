uint16_t XY( uint8_t x, uint8_t y)
{
  uint16_t i;
  
  if( filasInvertidas == false) {
    i = (y * COLUMNAS_MATRIX) + x;
  }
  if( filasInvertidas == true) {
    if( y & 0x01) {
      uint8_t reverseX = (COLUMNAS_MATRIX - 1) - x;
      i = (y * COLUMNAS_MATRIX) + reverseX;
    } else {
      i = (y * COLUMNAS_MATRIX) + x;
    }
  }
  
  return i;
}
